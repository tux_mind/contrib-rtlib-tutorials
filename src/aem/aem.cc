/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "config.h"
#include "simple_exc.h"
#include "utility.h"

#include <stdlib.h>

using namespace std;

// Tutorial messages file
ifstream msgs_file;

// This will contains the RTLIB services.
RTLIB_Services_t * rtlib;


int main(int argc, char *argv[]) {
	RTLIB_ExitCode result;
	const char *messages = BBQUE_TUTORIAL_PATH_DOCS"/aem.txt";

	clearScreen();
	msgs_file.open(messages);
	if (!msgs_file.is_open()) {
		std::cout << "Open tutorial messages file "
			<< messages << " FAILED" << std::endl;
		return -1;
	}

	TTR_MESSAGE("_SPLASH_BANNER", COLOR_LRED, STOP_NONE);
	TTR_MESSAGE("_SPLASH_MSG", COLOR_GRAY, STOP_PAUSE);
	clearScreen();
	TTR_MESSAGE("_INTRO_CLASS", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_PROG_CLASS", COLOR_WHITE, STOP_PAUSE);
	TTR_MESSAGE("_INTRO_INIT", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_PROG_INIT", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_INIT", COLOR_GRAY, STOP_SHOW);

	// First step: Init the communication with Barbeque RTRM
	RTLIB_Init(APP_NAME, &rtlib);

	if (!rtlib) {
		cout << "Error: Cannot find Barbeque RTRM running" << endl;
		return -1;
	}

	// --- A simple application with only one task to execute ---

	TTR_MESSAGE("_INTRO_EXC", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_PROG_EXC", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_EXC", COLOR_GRAY, STOP_SHOW);

	// The EXC;
	cout << " ... Registering a new Execution Context ... " << endl;
	SimpleEXC * myExeC = new SimpleEXC(EXC_NAME, RECIPE_NAME, rtlib);
	if (!myExeC) {
		cout << "Error: Unable to register the EXC" << endl;
		return -2;
	}

	TTR_MESSAGE("_INTRO_START", COLOR_GRAY, STOP_NONE);
	TTR_MESSAGE("_PROG_START", COLOR_WHITE, STOP_SHOW);

	// Let's start!
	cout << " ... Starting the EXC ... " << endl;
	result = myExeC->Start();
	if (result != RTLIB_OK) {
		cout << "Error: Unable to start the EXC "<< endl;
		return -4;
	}

	myExeC->WaitCompletion();
	sleep(2);
	clearScreen();

	// It is a better practice to release the EXC as soon as it is not more
	// needed. Indeed, this ensure to free-up all the EXC assigned
	// resources as soon as possible.
	delete myExeC;

	TTR_MESSAGE("_OUTRO_START", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_PROG_ONSTP_", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_ONSTP_", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_PROG_ONRUN", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_ONRUN", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_PROG_ONCFG", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_ONCFG", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_PROG_ONSSP", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_ONSSP", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_PROG_ONMON", COLOR_WHITE, STOP_NONE);
	TTR_MESSAGE("_OUTRO_ONMON", COLOR_GRAY, STOP_PAUSE);
	TTR_MESSAGE("_INTRO_EXIT", COLOR_GRAY, STOP_PAUSE);
	clearScreen();

	TTR_MESSAGE("_BYE", COLOR_LRED, STOP_NONE);

	return EXIT_SUCCESS;
}


